# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the akonadi package.
# Wantoyèk <wantoyek@gmail.com>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: akonadi\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-02 00:55+0000\n"
"PO-Revision-Date: 2022-06-14 22:34+0700\n"
"Last-Translator: Wantoyèk <wantoyek@gmail.com>\n"
"Language-Team: Indonesian <kde-i18n-doc@kde.org>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.3\n"

#: knutresource.cpp:60
#, kde-format
msgid "No data file selected."
msgstr "Tidak ada file data yang dipilih."

#: knutresource.cpp:78
#, kde-format
msgid "File '%1' loaded successfully."
msgstr "File '%1' berhasil dimuat."

#: knutresource.cpp:105
#, kde-format
msgid "Select Data File"
msgstr "Pilih File Data"

#: knutresource.cpp:107
#, kde-format
msgctxt "Filedialog filter for Akonadi data file"
msgid "Akonadi Knut Data File"
msgstr "File Data Akonadi Knut"

#: knutresource.cpp:149 knutresource.cpp:169 knutresource.cpp:322
#, kde-format
msgid "No item found for remoteid %1"
msgstr "Tidak ada item yang ditemukan untuk remoteid %1"

#: knutresource.cpp:187
#, kde-format
msgid "Parent collection not found in DOM tree."
msgstr "Koleksi induk tidak ditemukan dalam ranting DOM."

#: knutresource.cpp:195
#, kde-format
msgid "Unable to write collection."
msgstr "Tidak dapat menulis koleksi."

#: knutresource.cpp:207
#, kde-format
msgid "Modified collection not found in DOM tree."
msgstr "Koleksi yang dimodifikasi tidak ditemukan di ranting DOM."

#: knutresource.cpp:237
#, kde-format
msgid "Deleted collection not found in DOM tree."
msgstr "Koleksi yang dihapus tidak ditemukan di ranting DOM."

#: knutresource.cpp:251 knutresource.cpp:309 knutresource.cpp:316
#, kde-format
msgid "Parent collection '%1' not found in DOM tree."
msgstr "Koleksi induk '%1' tidak ditemukan dalam ranting DOM."

#: knutresource.cpp:259 knutresource.cpp:329
#, kde-format
msgid "Unable to write item."
msgstr "Tidak dapat menulis item."

#: knutresource.cpp:273
#, kde-format
msgid "Modified item not found in DOM tree."
msgstr "Item yang dimodifikasi tidak ditemukan di ranting DOM."

#: knutresource.cpp:288
#, kde-format
msgid "Deleted item not found in DOM tree."
msgstr "Item yang dihapus tidak ditemukan di ranting DOM."
