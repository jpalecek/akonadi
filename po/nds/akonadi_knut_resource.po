# translation of akonadi_knut_resource.po to Low Saxon
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Manfred Wiese <m.j.wiese@web.de>, 2009, 2010.
# Sönke Dibbern <s_dibbern@web.de>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: akonadi_knut_resource\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-02 00:55+0000\n"
"PO-Revision-Date: 2010-07-29 15:51+0200\n"
"Last-Translator: Manfred Wiese <m.j.wiese@web.de>\n"
"Language-Team: Low Saxon <kde-i18n-nds@kde.org>\n"
"Language: nds\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.1\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: knutresource.cpp:60
#, kde-format
msgid "No data file selected."
msgstr "Keen Datendatei utsöcht"

#: knutresource.cpp:78
#, kde-format
msgid "File '%1' loaded successfully."
msgstr "Datei \"%1\" mit Spood laadt."

#: knutresource.cpp:105
#, kde-format
msgid "Select Data File"
msgstr "Datendatei utsöken"

#: knutresource.cpp:107
#, kde-format
msgctxt "Filedialog filter for Akonadi data file"
msgid "Akonadi Knut Data File"
msgstr "Akonadi-Knut-Datendatei"

#: knutresource.cpp:149 knutresource.cpp:169 knutresource.cpp:322
#, kde-format
msgid "No item found for remoteid %1"
msgstr "För feern ID \"%1\" lett sik keen Indrag finnen."

#: knutresource.cpp:187
#, kde-format
msgid "Parent collection not found in DOM tree."
msgstr "Överornt Sammeln nich binnen DOM-Boom funnen."

#: knutresource.cpp:195
#, kde-format
msgid "Unable to write collection."
msgstr "Sammeln lett sik nich schrieven."

#: knutresource.cpp:207
#, kde-format
msgid "Modified collection not found in DOM tree."
msgstr "Ännert Sammeln nich binnen DOM-Boom funnen."

#: knutresource.cpp:237
#, kde-format
msgid "Deleted collection not found in DOM tree."
msgstr "Wegdaan Sammeln nich binnen DOM-Boom funnen."

#: knutresource.cpp:251 knutresource.cpp:309 knutresource.cpp:316
#, kde-format
msgid "Parent collection '%1' not found in DOM tree."
msgstr "Överornt Sammeln \"%1\" nich binnen DOM-Boom funnen."

#: knutresource.cpp:259 knutresource.cpp:329
#, kde-format
msgid "Unable to write item."
msgstr "Indrag lett sik nich schrieven."

#: knutresource.cpp:273
#, kde-format
msgid "Modified item not found in DOM tree."
msgstr "Ännert Indrag nich binnen DOM-Boom funnen."

#: knutresource.cpp:288
#, kde-format
msgid "Deleted item not found in DOM tree."
msgstr "Wegdaan Indrag nich binnen DOM-Boom funnen."

#~ msgid "Path to the Knut data file."
#~ msgstr "Padd na de Knut-Datendatei"

#~ msgid "Do not change the actual backend data."
#~ msgstr "Bitte de aktuellen Hülpprogramm-Daten nich ännern."
